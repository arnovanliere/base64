package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncode(t *testing.T) {
	t.Run("Exact block size match", func(t *testing.T) {
		val, err := Encode([]byte("Man"))
		assert.NoError(t, err, "error encoding: %v", err)
		assert.Equal(t, "TWFu", val)
	})
	t.Run("One block padding", func(t *testing.T) {
		val, err := Encode([]byte("any carnal pleasure."))
		assert.NoError(t, err, "error encoding: %v", err)
		assert.Equal(t, "YW55IGNhcm5hbCBwbGVhc3VyZS4=", val)
	})
	t.Run("Two blocks padding", func(t *testing.T) {
		val, err := Encode([]byte("any carnal pleasure"))
		assert.NoError(t, err, "error encoding: %v", err)
		assert.Equal(t, "YW55IGNhcm5hbCBwbGVhc3VyZQ==", val)
	})
}
