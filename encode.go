package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// Encode takes in normal data and encodes it according to base64 format.
// It returns the encoded string and an error if an error occurred while encoding.
func Encode(data []byte) (string, error) {
	binStr := ""
	encoded := ""

	// Remove all newlines
	dataStr := strings.ReplaceAll(string(data), "\n", "")

	// Create string with binary data
	for _, char := range dataStr {
		// Create binary string from character
		binary := strconv.FormatInt(int64(char), 2)
		// Make string length 8 with 0's upfront
		padded := fmt.Sprintf("%08s", binary)
		binStr += padded
	}

	// Add padding to end of string so each block is size 6
	lastBlock := len(binStr) % 6
	if lastBlock != 0 {
		for i := 0; i < 6 - lastBlock; i++ {
			binStr += "0"
		}
	}

	// Read each 6 bytes and add the corresponding character to the encoded string
	for i := 0; i < len(binStr); i += 6 {
		binary := binStr[i : i+6]
		val, exists := index[binary]
		if !exists {
			return "", errors.New(fmt.Sprintf("invalid binary string found: %s", binary))
		}
		encoded += val
	}

	// Add padding to end
	lastBlockLength := len(dataStr) % 3
	if lastBlockLength != 0 {
		for i := 0; i < 3 - lastBlockLength; i++ {
			encoded += "="
		}
	}

	return encoded, nil
}
