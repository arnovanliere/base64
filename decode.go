package main

import (
	"errors"
	"fmt"
	"strconv"
)

// Decode takes in base64-encoded data and decodes it according to base64 format.
// It returns the decoded string and an error if an error occurred while decoding.
func Decode(data string) (string, error) {
	if (len(data)*6)%8 != 0 {
		return "", errors.New("invalid base64 string")
	}

	decoded := ""
	base64Str := ""

	// Read all base64 characters to binary
	for _, char := range data {
		if char == '=' {
			continue
		}
		binStr, exists := getBinary(string(char))
		if !exists {
			return "", errors.New(fmt.Sprintf("invalid base64 char found: %s", string(char)))
		}
		base64Str += binStr
	}

	// Append 0's so each block size is 8
	lastBlockSize := len(base64Str) %8
	if lastBlockSize != 0 {
		for i := 0; i < 8 - lastBlockSize; i++ {
			base64Str += "0"
		}
	}

	// Read every 8 bytes and get character
	for i := 0; i < len(base64Str); i += 8 {
		binStr := base64Str[i : i+8]
		// Convert to binary number
		bin, err := strconv.ParseInt(binStr, 2, 64)
		if err != nil {
			return "", err
		}
		if bin != 0 {
			// Append character
			decoded += string(rune(bin))
		}
	}

	return decoded, nil
}
