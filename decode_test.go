package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDecode(t *testing.T) {
	t.Run("Exact block size match", func(t *testing.T) {
		val, err := Decode("TWFu")
		assert.NoError(t, err, "error decoding: %v", err)
		assert.Equal(t, "Man", val)
	})
	t.Run("One block padding", func(t *testing.T) {
		val, err := Decode("YW55IGNhcm5hbCBwbGVhc3VyZS4=")
		assert.NoError(t, err, "error decoding: %v", err)
		assert.Equal(t, "any carnal pleasure.", val)
	})
	t.Run("Two blocks padding", func(t *testing.T) {
		val, err := Decode("YW55IGNhcm5hbCBwbGVhc3VyZQ==")
		assert.NoError(t, err, "error decoding: %v", err)
		assert.Equal(t, "any carnal pleasure", val)
	})
}
